package com.example.archivoappuser.Activities.Authentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.archivoappuser.Activities.HomeActivity;
import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Models.User;
import com.example.archivoappuser.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_USER;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_USERS;

public class RegisterActivity extends AppCompatActivity {


    private static final String TAG = "RegistrationActivity";
    private TextInputEditText edt_email, edt_password, edt_username, edt_personID;
    private TextView tv_back;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private User user;
    private Button btn_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        //Init Firebase
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        databaseReference = mFirebaseDatabase.getReference(KEY_TABLE_USERS);
        btn_create = findViewById(R.id.btn_action);
        //  checkValidation();

        if (getIntent() != null) {
            user = Common.CurrentUser;
            edt_username = findViewById(R.id.et_person_name);
            edt_username.setEnabled(false);
            edt_password = findViewById(R.id.et_person_password);
            edt_password.setEnabled(false);
            edt_email = findViewById(R.id.et_person_Email);
            edt_personID = findViewById(R.id.et_person_id);
            edt_personID.setEnabled(false);
            tv_back = findViewById(R.id.tv_back);

            edt_personID.setText(user.getUserId());

            edt_email.setText(user.getEmail());
            edt_username.setText(user.getUserName());
            edt_password.setText(user.getPassword());
            btn_create = (Button) findViewById(R.id.btn_create);

            btn_create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (CheckEmailValidation(edt_email)) {
                        mAuth.createUserWithEmailAndPassword(edt_email.getText().toString(), user.getPassword())
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            mFirebaseUser = mAuth.getCurrentUser();
                                            user.setUserUid(mFirebaseUser.getUid());
                                            user.setOnline(true);
                                            user.setEmail(edt_email.getText().toString());
                                            UpdateActivation(mAuth.getCurrentUser().getUid(), user.getEmail());
                                            final DatabaseReference user_info = mFirebaseDatabase.getReference(KEY_TABLE_USERS)
                                                    .child(mFirebaseUser.getUid());
                                            user_info.setValue(user)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Common.CurrentUser = user;
                                                                Log.d(TAG, "onComplete: user Creation" + user.toString());
                                                                FirebaseMessaging.getInstance().subscribeToTopic(Common.CurrentUser.getGroupId());
                                                                Log.d(TAG, "onComplete: " + task.getResult());
                                                                String delete = getIntent().getStringExtra("KEY");
                                                                mFirebaseDatabase.getReference(KEY_TABLE_USERS)
                                                                        .child(delete).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        Log.d(TAG, "onComplete: task" + task.getException());
                                                                        Intent i = new Intent(new Intent(RegisterActivity.this, HomeActivity.class));
                                                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                        startActivity(i);
                                                                        finish();
                                                                    }
                                                                });

                                                            } else if (!task.isSuccessful()) {
                                                                Log.d(TAG, "onComplete: !task.isSuccessful() " + task.getResult());
                                                            }
                                                        }
                                                    });


                                        } else {
                                            Toast.makeText(RegisterActivity.this, "Error Configuring data \nPlease contact Admin", Toast.LENGTH_SHORT).show();

                                        }
                                    }
                                });
                    }
                }
            });


        }
        //init back tv
        tv_back = (TextView) findViewById(R.id.tv_back);

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Back is pressed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private boolean CheckEmailValidation(TextInputEditText edt_email) {

        //Checking Email Validation
        String email = edt_email.getText().toString();
        if (email.isEmpty()) {
            edt_email.setError(getString(R.string.input_error_email));
            edt_email.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edt_email.setError(getString(R.string.input_error_email_invalid));
            edt_email.requestFocus();
            return false;
        } else {
            return true;
        }

    }

    public void checkValidation() {
        String email, password, name;
        name = edt_username.getText().toString();
        if (name.isEmpty()) {
            edt_username.setError(getString(R.string.input_error_name));
            edt_username.requestFocus();
            return;
        }

        //Checking Email Validation
        email = edt_email.getText().toString();
        if (email.isEmpty()) {
            edt_email.setError(getString(R.string.input_error_email));
            edt_email.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edt_email.setError(getString(R.string.input_error_email_invalid));
            edt_email.requestFocus();
            return;
        }

        //Checking Password Validation
        password = edt_password.getText().toString();
        if (password.isEmpty()) {
            edt_password.setError(getString(R.string.input_error_password));
            edt_password.requestFocus();
            return;
        }

        if (password.length() < 6) {
            edt_password.setError(getString(R.string.input_error_password_length));
            edt_password.requestFocus();
            return;
        }

    }

    void UpdateActivation(final String userId, String email) {
        try {
            FirebaseDatabase mfb = FirebaseDatabase.getInstance();
            DatabaseReference dbb = mfb.getReference(KEY_TABLE_USERS).child(userId);
            dbb.child("email").setValue(email);
            dbb.child("online").setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "onSuccess: user is online = " + true);
                }
            });
        } catch (Exception e) {
            Log.d(TAG, "UpdateActivation: " + e.getMessage());
        }
    }


}
