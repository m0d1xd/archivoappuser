package com.example.archivoappuser.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.archivoappuser.Activities.MyTasksActivity.GroupTasks;
import com.example.archivoappuser.Activities.MyTasksActivity.PersonTasks;
import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.MainActivity;
import com.example.archivoappuser.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.example.archivoappuser.Common.Common.KEY_TABLE_TOKENS;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_USERS;

public class HomeActivity extends AppCompatActivity {


    private TextView tv_welcome, tv_receivedTasks, tv_groupTasks, tv_logout;

    private final String TAG = "HomeActivity";
    private DatabaseReference mDatabaseReference, mDatabaseRefrenceTokens;
    private FirebaseDatabase mFirebaseDatabe;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        tv_welcome = findViewById(R.id.tv_welcome);
        tv_receivedTasks = findViewById(R.id.tv_receivedtask);
        tv_logout = findViewById(R.id.logout);
        tv_groupTasks = findViewById(R.id.tv_groupTasks);

        mFirebaseDatabe = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabe.getReference(KEY_TABLE_USERS);
        mDatabaseRefrenceTokens = mFirebaseDatabe.getReference(KEY_TABLE_TOKENS);
        if (Common.CurrentUser != null && FirebaseAuth.getInstance().getCurrentUser() != null) {
            tv_welcome.setText(getString(R.string.welcome) + Common.CurrentUser.getUserName());
            tv_logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogOut();
                }
            });

            tv_receivedTasks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(HomeActivity.this, PersonTasks.class);
                    startActivity(i);
                }
            });
            tv_groupTasks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(HomeActivity.this, GroupTasks.class);
                    startActivity(i);
                }
            });
        } else {
            startActivity(new Intent(HomeActivity.this, MainActivity.class));
            finish();
        }


    }


    //TODO Fix the signOut Method

    private void LogOut() {
        try {
            Common.CurrentUser = null;
            Intent i = new Intent(HomeActivity.this, MainActivity.class);
            String key = FirebaseAuth.getInstance().getCurrentUser().getUid();
            mDatabaseReference.child(key).child("online").setValue(false);
            FirebaseAuth.getInstance().signOut();
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(getApplicationContext(), "Good Bye", Toast.LENGTH_SHORT).show();
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.d(TAG, "LogOut: " + e.getMessage());
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

    }


}
