package com.example.archivoappuser.Activities.MyTasksActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Interface.ItemClickListener;
import com.example.archivoappuser.Models.GroupsTaskTime;
import com.example.archivoappuser.Models.GroupsTasks;
import com.example.archivoappuser.R;
import com.example.archivoappuser.ViewHolder.MyTasksViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.example.archivoappuser.Common.Common.KEY_FIREBASE_TASK_ID;
import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.archivoappuser.Common.Common.KEY_QUERY_FILTER_Group_ID;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_PENDING;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_STARTED;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_GROUP_TASK;

public class GroupTasks extends AppCompatActivity {
    private static final String TAG = "GroupTasks";

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private FirebaseRecyclerOptions<GroupsTasks> MyTaskListFirebaseRecyclerOptions;

    private FirebaseRecyclerAdapter<GroupsTasks, MyTasksViewHolder> adapter;

    private TextView tv_Welcome;

    private RecyclerView rv_GroupTasks;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_tasks);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_GROUP_TASK);

        rv_GroupTasks = findViewById(R.id.rv_GroupTasks);

        rv_GroupTasks.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);

        rv_GroupTasks.setLayoutManager(layoutManager);

        Query query = databaseReference.orderByChild(KEY_QUERY_FILTER_Group_ID)
                .equalTo(Common.CurrentUser.getGroupId());


        MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<GroupsTasks>()
                .setQuery(query, GroupsTasks.class)
                .build();


        adapter = new FirebaseRecyclerAdapter<GroupsTasks, MyTasksViewHolder>(MyTaskListFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull MyTasksViewHolder holder,
                                            int position,
                                            @NonNull final GroupsTasks model) {
                Log.d(TAG, "onBindViewHolder: " + model.toString());
                holder.tv_username.setText(Common.CurrentUser.getUserName());
                holder.tv_tasksummary.setText(model.getTask());
                holder.tv_time.setText(Common.getDate(model.getDeadline()));
                try {
                    GroupsTaskTime time = model.getTimeTable().get(Common.CurrentUser.getUserUid());

                    if (time.getStartTime() == 0) {
                        holder.tv_taskstatus.setText(KEY_STATUS_PENDING);
                    } else {
                        if (time.getFinishTime() == 0) {
                            holder.tv_taskstatus.setText(KEY_STATUS_STARTED);
                        } else {
                            holder.tv_taskstatus.setText(Common.CheckTimerStatus(model.getDeadline(), time.getFinishTime()));
                        }
                    }
                } catch (Exception e) {
                    GroupsTaskTime time = new GroupsTaskTime();
                    if (model.getTimeTable() == null) {
                        DatabaseReference ref = firebaseDatabase.getReference(KEY_TABLE_GROUP_TASK)
                                .child(adapter.getRef(position).getKey())
                                .child("timeTable")
                                .child(Common.CurrentUser.getUserUid());
                        ref.setValue(time);
                        Log.d(TAG, "onBindViewHolder: getUsers == null" + adapter.getRef(position).getKey());
                    }

                    Log.d(TAG, "onBindViewHolder: " + e.getMessage());

                }
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent i = new Intent(GroupTasks.this, GroupTaskDetailsActivity.class);
                        i.putExtra(KEY_INTENT_EXTRA_MODEL, model);
                        i.putExtra("stats", model.getTimeTable().get(Common.CurrentUser.getUserUid()));
                        i.putExtra(KEY_FIREBASE_TASK_ID, adapter.getRef(position).getKey());
                        startActivity(i);

                    }
                });

            }

            @NonNull
            @Override
            public MyTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.rv_task_list_layout, parent, false);

                return new MyTasksViewHolder(itemView);
            }
        };


        adapter.startListening();
        adapter.notifyDataSetChanged();
        rv_GroupTasks.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }
}
