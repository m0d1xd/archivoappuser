package com.example.archivoappuser.Activities.MyTasksActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Interface.ItemClickListener;
import com.example.archivoappuser.Models.MyTasks;
import com.example.archivoappuser.R;
import com.example.archivoappuser.ViewHolder.MyTasksViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.example.archivoappuser.Common.Common.KEY_FIREBASE_TASK_ID;
import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TASK;

public class PersonTasks extends AppCompatActivity {

    private static final String TAG = "PerosnTasks";
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private FirebaseRecyclerOptions<MyTasks> MyTaskListFirebaseRecyclerOptions;

    private FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder> adapter;

    private TextView tv_Welcome;

    private RecyclerView rv_UserAssignedTasks;
    private RecyclerView.LayoutManager layoutManager;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_tasks);

        tv_Welcome = findViewById(R.id.tv_welcome_allactivity);

        if (Common.CurrentUser != null) {
            tv_Welcome.setText("Welcome \n" + Common.CurrentUser.getUserName());
            Log.d(TAG, "onCreate: " + Common.CurrentUser.toString());


            rv_UserAssignedTasks = findViewById(R.id.rv_UserTaskList);

            rv_UserAssignedTasks.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(this);
            rv_UserAssignedTasks.setLayoutManager(layoutManager);


            //Init Firebase
            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);

//
//            Query query = databaseReference.startAt(System.currentTimeMillis(), "")
//                    .orderByChild(Common.KEY_FIREBASE_TASK_USER_UID)
//                    .equalTo(Common.CurrentUser.getUserUid());

            Query query = databaseReference.orderByChild(Common.KEY_FIREBASE_TASK_USER_UID)
                    .equalTo(Common.CurrentUser.getUserUid());

            MyTaskListFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<MyTasks>()
                    .setQuery(query, MyTasks.class)
                    .build();

            adapter = new FirebaseRecyclerAdapter<MyTasks, MyTasksViewHolder>(MyTaskListFirebaseRecyclerOptions) {
                @Override
                protected void onBindViewHolder(@NonNull MyTasksViewHolder holder, int position, @NonNull final MyTasks model) {
                    holder.tv_username.setText(Common.CurrentUser.getUserName());
                    holder.tv_tasksummary.setText(model.getTask());
                    holder.tv_taskstatus.setText(model.getStatus());
                    holder.tv_time.setText(Common.getDate(model.getMillis()));

                    holder.setItemClickListener(new ItemClickListener() {
                        @Override
                        public void onClick(View view, int position, boolean isLongClick) {
                            Intent i = new Intent(PersonTasks.this, TaskDetailsActivity.class);
                            i.putExtra(KEY_INTENT_EXTRA_MODEL, model);
                            i.putExtra(KEY_FIREBASE_TASK_ID, adapter.getRef(position).getKey());
                            startActivity(i);
                            Log.d(TAG, "onClick: " + adapter.getRef(position).getKey());

                        }
                    });
                    Log.d("Test", "onBindViewHolder: " + model.toString());
                }

                @NonNull
                @Override
                public MyTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                    View itemView = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.rv_task_list_layout, parent, false);


                    return new MyTasksViewHolder(itemView);
                }
            };

            adapter.startListening();
            adapter.notifyDataSetChanged();
            rv_UserAssignedTasks.setAdapter(adapter);

        }
    }
}
