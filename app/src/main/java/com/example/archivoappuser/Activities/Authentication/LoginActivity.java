package com.example.archivoappuser.Activities.Authentication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.archivoappuser.Activities.HomeActivity;
import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.MainActivity;
import com.example.archivoappuser.Models.NotificationModel.Token;
import com.example.archivoappuser.Models.User;
import com.example.archivoappuser.R;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.example.archivoappuser.Common.Common.CurrentUser;
import static com.example.archivoappuser.Common.Common.KEY_QUERY_FILTER_USERS_ID;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TOKENS;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_USERS;

public class LoginActivity extends AppCompatActivity {


    private static final String TAG = "LoginActivity";
    private TextInputEditText edt_email, edt_password;
    private Button btn_Login;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private FirebaseUser firebaseUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private ValueEventListener userDataListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference(KEY_TABLE_USERS);

        Log.d(TAG, "onCreate: " + Common.CurrentUser.toString());

        final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
        mDialog.setMessage("Please Wait  ...");

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    if (Common.CurrentUser != null) {
                        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                        FirebaseMessaging.getInstance().subscribeToTopic(Common.CurrentUser.getGroupId());
                        mDatabaseReference.child(firebaseUser.getUid()).child("online").setValue(true);
                        UpdateToken(FirebaseInstanceId.getInstance().getToken());
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };

        mAuth = FirebaseAuth.getInstance();

        edt_email = findViewById(R.id.edt_email);

        edt_email.setText(Common.CurrentUser.getUserId());

        edt_password = findViewById(R.id.edt_password);

        btn_Login = findViewById(R.id.btn_Login);
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.show();
                checkValidation();
                String Password = edt_password.getText().toString();
                if (Password.isEmpty()) {
                    edt_password.setError(getString(R.string.input_error_password));
                    edt_password.requestFocus();
                    return;
                } else if (Password.length() < 6) {
                    edt_password.setError(getString(R.string.input_error_password_length));
                    edt_password.requestFocus();
                    return;
                } else if (Common.CurrentUser != null) {
                    mAuth.signInWithEmailAndPassword(Common.CurrentUser.getEmail(), edt_password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        firebaseUser = mAuth.getCurrentUser();
                                        if (firebaseUser != null) {
                                            try {
                                                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                UpdateToken(FirebaseInstanceId.getInstance().getToken());
                                                startActivity(i);
                                                finish();
                                            } catch (Exception e) {
                                                Log.d(TAG, "onComplete: Button_login firebaseUser null");
                                            }

                                        }

                                    } else if (!task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(),
                                                "Login Error please Check Email or password",
                                                Toast.LENGTH_LONG).show();
                                        Log.d(TAG, "onComplete: " + task.getException());
                                        mDialog.dismiss();
                                    }

                                    mDialog.dismiss();
                                }
                            });
                }
            }
        });


    }


    private void UpdateToken(String token) {
        DatabaseReference tokens = mFirebaseDatabase.getReference(KEY_TABLE_TOKENS);
        Token tokenData = new Token(token, "false");
        tokens.child(Common.CurrentUser.getUserUid()).setValue(tokenData);
        if (Common.CurrentUser != null) {
            DatabaseReference online = mFirebaseDatabase.getReference(KEY_TABLE_USERS)
                    .child(Common.CurrentUser.getUserUid())
                    .child("online");
            online.setValue(true);
        }
    }

    public void checkValidation() {
        String password;

        //Checking Password Validation
        password = edt_password.getText().toString();
        if (password.isEmpty()) {
            edt_password.setError(getString(R.string.input_error_password));
            edt_password.requestFocus();
            return;
        }

        if (password.length() < 6) {
            edt_password.setError(getString(R.string.input_error_password_length));
            edt_password.requestFocus();
            return;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth == null) {
            mAuth.addAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuth != null)
            mAuth.removeAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuth != null) {
            mAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}
