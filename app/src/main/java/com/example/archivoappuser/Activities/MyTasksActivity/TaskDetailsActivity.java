package com.example.archivoappuser.Activities.MyTasksActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Models.MyTasks;
import com.example.archivoappuser.Models.NotificationModel.FCMResponce;
import com.example.archivoappuser.Models.NotificationModel.Notification;
import com.example.archivoappuser.Models.NotificationModel.Sender;
import com.example.archivoappuser.Models.NotificationModel.Token;
import com.example.archivoappuser.R;
import com.example.archivoappuser.Remote.APIServices;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.archivoappuser.Common.Common.CheckTimerStatus;
import static com.example.archivoappuser.Common.Common.Difference;
import static com.example.archivoappuser.Common.Common.KEY_FIREBASE_TASK_ID;
import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_COMPLETED_ON_TIME;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_FINISH;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_FINISHED;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_LATE_COMPLETE;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_PENDING;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_START;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_STARTED;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TASK;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TOKENS;

public class TaskDetailsActivity extends AppCompatActivity implements LocationListener {

    private static final String TAG = "TaskDetailsActivity";

    private TextView tv_welcome, tv_Task_details, tv_Status, tv_timer;

    private Button btn_action;

    private FirebaseDatabase firebaseDatabase;

    private DatabaseReference databaseReference;

    private APIServices mServices;

    private String TaskID;
    private MyTasks myTasks;

    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private LocationManager locationManager;
    private String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        tv_welcome = findViewById(R.id.tv_welcome_allactivity);
        tv_Task_details = findViewById(R.id.tvtask);
        tv_Status = findViewById(R.id.tvstatus);
        btn_action = findViewById(R.id.btn_action);

        tv_timer = findViewById(R.id.tvtimer);

        firebaseDatabase = FirebaseDatabase.getInstance();

        databaseReference = firebaseDatabase.getReference(KEY_TABLE_TASK);

        mServices = Common.getFCMService();

        if (getIntent() != null) {

            TaskID = getIntent().getStringExtra(KEY_FIREBASE_TASK_ID);
            myTasks = getIntent().getParcelableExtra(KEY_INTENT_EXTRA_MODEL);


            tv_welcome.setText(getString(R.string.welcome) + Common.CurrentUser.getUserName());

            final Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            Thread.sleep(1000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateTextView();
                                }

                                private void updateTextView() {
                                    Date d2 = new Date(System.currentTimeMillis());
                                    if (myTasks.getStartTime() != 0) {
                                        tv_timer.setText(Difference(new Date(myTasks.getStartTime()), d2));

                                    }
                                }
                            });
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };


            tv_Task_details.setText(myTasks.getTask());

            tv_Status.setText(myTasks.getStatus());


            String CurrentStatus = myTasks.getStatus();

            if (CurrentStatus.equals(KEY_STATUS_PENDING)) {
                btn_action.setText(KEY_STATUS_START);
            } else if (CurrentStatus.equals(KEY_STATUS_STARTED)) {
                btn_action.setText(KEY_STATUS_FINISH);
                t.start();
                initTracing();
            } else if (CurrentStatus.equals(KEY_STATUS_FINISHED) ||
                    CurrentStatus.equals(KEY_STATUS_COMPLETED_ON_TIME) ||
                    CurrentStatus.equals(KEY_STATUS_LATE_COMPLETE)) {
                tv_Status.setText(CurrentStatus);
                btn_action.setVisibility(View.GONE);
                tv_timer.setVisibility(View.GONE);
            }

            //TODO implement the starting tasks and  edit
            //The user when starts the app will update the task startTime in the database.
            //then also nesnd a notification to the admin
            btn_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateTask(myTasks, TaskID, t);
                    sendNotification(Common.CurrentUser.getUserName(),
                            KEY_STATUS_STARTED + " The Task " + myTasks.getTask());

                }
            });
        }
    }

    private void endTracing() {

    }

    private void initTracing() {
        checkLocationPermission();
        // Get the location manager
        if (myTasks.getStatus().equals(KEY_STATUS_STARTED)) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // Define the criteria how to select the locatioin provider -> use
            // default
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);


            Location location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider, 1000, 10, this);

            // Initialize the location fields
            if (location != null) {
                onLocationChanged(location);
            }
        }
    }


    private void sendNotification(final String Key, final String Operation) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference(KEY_TABLE_TOKENS);
        tokens.orderByChild("serverToken").equalTo("true")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Token token = ds.getValue(Token.class);
                            Log.d(TAG, "onDataChange: " + token.toString());
                            //Making raw Payload
                            Notification notification = new Notification(Key + " Has " + Operation, "Archivo App");
                            Sender content = new Sender(token.getToken(), notification);
                            mServices.sendNotification(content).enqueue(new Callback<FCMResponce>() {
                                @Override
                                public void onResponse(@NonNull Call<FCMResponce> call, @NonNull Response<FCMResponce> response) {
                                    assert response.body() != null;
                                    if (response.body().success == 1) {
                                        Toast.makeText(TaskDetailsActivity.this, "Task Started Successfully", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");
                                        Log.d(TAG, "onResponse: " + response.toString());
                                    } else {
                                        Toast.makeText(TaskDetailsActivity.this, "Task Started Successfully", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly" + response.body().toString());
                                    }
                                }

                                @Override
                                public void onFailure(Call<FCMResponce> call, Throwable t) {
                                    Log.d(TAG, "onFailure: " + t.getMessage());
                                }
                            });
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(TAG, "onCancelled: " + databaseError.getDetails());
                    }

                });
    }

    private void UpdateTask(final MyTasks myTask, final String key, Thread t) {
        if (myTask.getStartTime() == 0) {
            long startTime = System.currentTimeMillis();
            myTask.setStatus(KEY_STATUS_STARTED);
            myTask.setStartTime(startTime);
            tv_Status.setText(KEY_STATUS_STARTED);
            btn_action.setText(KEY_STATUS_FINISH);
            sendNotification(Common.CurrentUser.getUserName(), KEY_STATUS_STARTED);
            //TODO add gps Tracing here
            initTracing();
            t.start();

        } else if (myTask.getStartTime() != 0) {
            long finishTime = System.currentTimeMillis();
            String Status = CheckTimerStatus(myTask.getMillis(), finishTime);
            if (myTask.getFinishTime() == 0) {
                myTask.setStatus(Status);
                myTask.setFinishTime(finishTime);
                //TODO FIX End Tracing
                endTracing();
                tv_Status.setText(Status);
                btn_action.setVisibility(View.GONE);
                tv_timer.setVisibility(View.GONE);
                sendNotification(Common.CurrentUser.getUserName(), KEY_STATUS_FINISHED);
            } else {
                tv_Status.setText(Status);
                btn_action.setVisibility(View.GONE);

            }

        }


        //TODO add | update  FirebaseDatabase

        databaseReference.child(key).setValue(myTask)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(TAG, "onComplete: 237 " + task.isSuccessful());
                    }
                });


    }


    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("give permission")
                        .setMessage("give permission message")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(TaskDetailsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(TaskDetailsActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        1);
            }
        }
    }

    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case 1: {
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                        locationManager.requestLocationUpdates(provider, 1000, 10, this);
//                    }
//                } else {
//                    Toast.makeText(getApplicationContext(), "Please provide the permission", Toast.LENGTH_LONG).show();
//                }
//                break;
//            }
//        }
//    }
    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(TaskDetailsActivity.this,
                            "permission was granted, :)",
                            Toast.LENGTH_LONG).show();
                    initTracing();

                } else {
                    Toast.makeText(TaskDetailsActivity.this,
                            "permission denied, ...:(",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("StartedTasks");
            GeoFire geoFire = new GeoFire(ref);
            if ((myTasks.getStatus().equals(KEY_STATUS_FINISHED) ||
                    myTasks.getStatus().equals(KEY_STATUS_COMPLETED_ON_TIME) ||
                    myTasks.getStatus().equals(KEY_STATUS_LATE_COMPLETE))) {
                geoFire.removeLocation(Common.CurrentUser.getUserUid());
            } else {
                geoFire.setLocation(Common.CurrentUser.getUserUid(), new GeoLocation(location.getLatitude(), location.getLongitude()));
                Log.d(TAG, "onLocationChanged: " + geoFire.toString());
            }
        } catch (Exception e) {
            Log.d(TAG, "onLocationChanged: " + e.getMessage());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    //------------------------------------------------------------------------------
    //ref: Requesting Permissions at Run Time
    //http://developer.android.com/training/permissions/requesting.html
    //------------------------------------------------------------------------------
    private void getMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            //------------------------------------------------------------------------------
            ActivityCompat.requestPermissions(TaskDetailsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            return;
        }

    }
}
