package com.example.archivoappuser.Activities.MyTasksActivity;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Models.GroupsTaskTime;
import com.example.archivoappuser.Models.GroupsTasks;
import com.example.archivoappuser.Models.NotificationModel.FCMResponce;
import com.example.archivoappuser.Models.NotificationModel.Notification;
import com.example.archivoappuser.Models.NotificationModel.Sender;
import com.example.archivoappuser.Models.NotificationModel.Token;
import com.example.archivoappuser.R;
import com.example.archivoappuser.Remote.APIServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.archivoappuser.Common.Common.CheckTimerStatus;
import static com.example.archivoappuser.Common.Common.Difference;
import static com.example.archivoappuser.Common.Common.KEY_FIREBASE_TASK_ID;
import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_MODEL;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_FINISH;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_FINISHED;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_PENDING;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_START;
import static com.example.archivoappuser.Common.Common.KEY_STATUS_STARTED;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_GROUP_TASK;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TOKENS;

public class GroupTaskDetailsActivity extends AppCompatActivity {

    private static final String TAG = "GroupTaskDetailsActivity";
    GroupsTasks groupsTasks;
    String TaskID;
    private TextView tv_welcome, tv_Task_details, tv_Status, tv_timer;
    Button btn_submit;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;
    GroupsTaskTime time;

    private APIServices mServices;

    @SuppressLint("LongLogTag")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_task_details);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(KEY_TABLE_GROUP_TASK);

        if (getIntent() != null) {

            groupsTasks = getIntent().getParcelableExtra(KEY_INTENT_EXTRA_MODEL);
            TaskID = getIntent().getStringExtra(KEY_FIREBASE_TASK_ID);
            time = getIntent().getParcelableExtra("stats");

            if (time == null) {
                time = new GroupsTaskTime();
                databaseReference.child(TaskID).child("users").child(Common.CurrentUser.getUserUid()).setValue(time);
            }

            mServices = Common.getFCMService();

            tv_timer = findViewById(R.id.tvtimer);
            tv_welcome = findViewById(R.id.tv_welcome);
            tv_Task_details = findViewById(R.id.tv_task);
            tv_Task_details.setText(groupsTasks.getTask());
            tv_Status = findViewById(R.id.tv_status);
            // Log.d(TAG, "onCreate: " + time.toString());
            final Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            Thread.sleep(1000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateTextView();
                                }

                                private void updateTextView() {
                                    Date d2 = new Date(System.currentTimeMillis());
                                    if (time.getStartTime() != 0) {
                                        tv_timer.setText(Difference(new Date(time.getStartTime()), d2));
                                    }
                                }
                            });
                        }
                    } catch (InterruptedException e) {

                    }
                }
            };
            btn_submit = findViewById(R.id.btn_action);

            if (time.getStartTime() == 0) {
                tv_Status.setText(KEY_STATUS_PENDING);
                btn_submit.setText(KEY_STATUS_START);
            } else {

                if (time.getFinishTime() == 0) {
                    btn_submit.setText(KEY_STATUS_FINISH);
                    tv_Status.setText(KEY_STATUS_STARTED);
                    t.start();
                    //TODO FIx Tracing on Group Task
                    // initTracing();
                } else {
                    String CurrentStatus = Common.CheckTimerStatus(groupsTasks.getDeadline(), time.getFinishTime());
                    tv_Status.setText(CurrentStatus);
                    btn_submit.setVisibility(View.GONE);
                    tv_timer.setVisibility(View.GONE);
                }
            }

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UpdateGroupTaskForSingleUser(groupsTasks, Common.CurrentUser.getUserUid(), TaskID, t);

                }
            });

        }


    }

    private void sendNotification(final String Key, final String Operation) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference(KEY_TABLE_TOKENS);
        tokens.orderByChild("serverToken").equalTo("true")
                .addValueEventListener(new ValueEventListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            Token token = ds.getValue(Token.class);
                            Log.d(TAG, "onDataChange: " + token.toString());
                            //Making raw Payload
                            Notification notification = new Notification(Key + " From Group" +
                                    groupsTasks.getGroupId()
                                    + " Has " + Operation + " Group Task ID ", "Archivo App");
                            Sender content = new Sender(token.getToken(), notification);
                            mServices.sendNotification(content).enqueue(new Callback<FCMResponce>() {
                                @Override
                                public void onResponse(@NonNull Call<FCMResponce> call, @NonNull Response<FCMResponce> response) {
                                    assert response.body() != null;
                                    if (response.body().success == 1) {
                                        Toast.makeText(GroupTaskDetailsActivity.this, "Task Started Successfully", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly");
                                        Log.d(TAG, "onResponse: " + response.toString());
                                    } else {
                                        Toast.makeText(GroupTaskDetailsActivity.this, "Task Started Successfully", Toast.LENGTH_SHORT).show();
                                        Log.d(TAG, "onResponse: " + "Task Assigned Successfuly" + response.body().toString());
                                    }
                                }

                                @Override
                                public void onFailure(Call<FCMResponce> call, Throwable t) {
                                    Log.d(TAG, "onFailure: " + t.getMessage());
                                }
                            });
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("", "onCancelled: " + databaseError.getDetails());
                    }

                });
    }

    private void UpdateGroupTaskForSingleUser(GroupsTasks myTask, String useruid, String GroupTaskID, Thread t) {
        GroupsTaskTime currenttime = time;
        if (time.getStartTime() == 0) {
            long startTime = System.currentTimeMillis();
            currenttime.setStartTime(startTime);
            currenttime.setStatus(KEY_STATUS_STARTED);
            tv_Status.setText(KEY_STATUS_STARTED);
            btn_submit.setText(KEY_STATUS_FINISH);
            //  sendNotification(Common.CurrentUser.getUserName(), KEY_STATUS_STARTED);
            //TODO add gps Tracing here
            //initTracing();
            t.start();
        } else {
            long finishTime = System.currentTimeMillis();
            currenttime.setFinishTime(finishTime);
            String Status = CheckTimerStatus(myTask.getDeadline(), finishTime);
            currenttime.setStatus(Status);
            tv_Status.setText(Status);
            // endTracing();
            tv_timer.setVisibility(View.GONE);
            sendNotification(Common.CurrentUser.getUserName(), KEY_STATUS_FINISHED);
        }
        DatabaseReference update = databaseReference.child(GroupTaskID).child("timeTable").child(useruid);
        update.setValue(currenttime);
    }


}
