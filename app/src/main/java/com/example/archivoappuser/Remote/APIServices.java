package com.example.archivoappuser.Remote;

import com.example.archivoappuser.Models.NotificationModel.FCMResponce;
import com.example.archivoappuser.Models.NotificationModel.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

//TODO Replace the Authorization key with your Firebase Key for cloud messaging
public interface APIServices {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAA2QBIpYI:APA91bHxNt_kHX84fNVibz938MO5b466kAdeoUO1hIXJ-kzC-tdhwzK6T1k7xyFVg6fyV22iFbgHX6hUveL_Xxg647VwZShe_Gyn8tb-A9nSDiqEmYSWLiwpCbbqyN4O2LvOetJ_Lemj"
            }
    )


    @POST("fcm/send")
    Call<FCMResponce> sendNotification(@Body Sender body);

}
