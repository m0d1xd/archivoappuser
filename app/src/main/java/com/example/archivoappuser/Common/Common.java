package com.example.archivoappuser.Common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.archivoappuser.Models.User;
import com.example.archivoappuser.Remote.APIServices;
import com.example.archivoappuser.Remote.RetrofitClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Common {

    public static User CurrentUser;

    public static String dateFormat = "dd-MM-yyyy hh:mm";

    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

    //REGEX for ID Format
    final public static String KEY_REGEX_1 = "\\d{2}.\\d{3}.\\d{3}-[kK]";
    final public static String KEY_REGEX_2 = "\\d{2}.\\d{3}.\\d{3}-\\d";


    final public static String KEY_STATUS_COMPLETED_ON_TIME = "Completed on time";
    final public static String KEY_STATUS_LATE_COMPLETE = "Late completed";
    final public static String KEY_STATUS_STARTED = "Started";
    final public static String KEY_STATUS_START = "Start";
    final public static String KEY_STATUS_PENDING = "Pending";
    final public static String KEY_STATUS_FINISH = "Finish";
    final public static String KEY_STATUS_FINISHED = "Finished";
    final public static String KEY_INTENT_EXTRA_MODEL = "model";
    final public static String KEY_FIREBASE_TASK_ID = "TASKID";


    final public static String KEY_TABLE_USERS = "Users";

    final public static String KEY_TABLE_TASK = "TableTasks";
    final public static String KEY_TABLE_GROUP_TASK = "GroupTasks";

    final public static String KEY_TABLE_TOKENS = "Tokens";


    final public static String KEY_INTENT_EXTRA_USER = "user_model";


    final public static String KEY_QUERY_FILTER_USERS_ID = "userId";
    final public static String KEY_QUERY_FILTER_Group_ID = "groupId";


    final public static String KEY_FIREBASE_TOKENS = "Tokens";


    final public static String KEY_FIREBASE_TASK_USER_UID = "userUid";
    final public static String KEY_FIREBASE_TASK_USER_ID = "userId";
    final public static String KEY_FIREBASE_TASK_STATUS = "status";

    final public static String KEY_INTENT_ID = "ID";

    public static final String BASE_URL = "https://fcm.googleapis.com/";


    public static APIServices getFCMService() {
        return RetrofitClient.getClient(BASE_URL).create(APIServices.class);
    }

    public static Boolean IsConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static String getDate(long milliSeconds) {

        String dateFormat = "dd/MM/yyyy hh:mm:ss";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String CheckTimerStatus(long DurationTime, long finishTime) {
        String Delivery_status;

        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar.setTimeInMillis(DurationTime);
        calendar2.setTimeInMillis(finishTime);


        if (calendar2.before(calendar)) {
            Delivery_status = KEY_STATUS_COMPLETED_ON_TIME;
        } else {
            Delivery_status = KEY_STATUS_LATE_COMPLETE;
        }


        return Delivery_status;
    }


    public static String ConvertMilliSecondsToFormattedDate(String milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String Difference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        String date;
        if (elapsedDays == 0) {
            date = String.valueOf(elapsedHours) + ":" + String.valueOf(elapsedMinutes) + ":" + String.valueOf(elapsedSeconds);
        } else {
            date = String.valueOf(elapsedDays) +
                    " d " + String.valueOf(elapsedHours) + ":" + String.valueOf(elapsedMinutes) + ":" + String.valueOf(elapsedSeconds);
        }

        return date;
    }
}
