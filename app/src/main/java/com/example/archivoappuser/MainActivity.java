package com.example.archivoappuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.archivoappuser.Activities.Authentication.LoginActivity;
import com.example.archivoappuser.Activities.Authentication.RegisterActivity;
import com.example.archivoappuser.Activities.HomeActivity;
import com.example.archivoappuser.Common.Common;
import com.example.archivoappuser.Models.NotificationModel.Token;
import com.example.archivoappuser.Models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.example.archivoappuser.Common.Common.KEY_INTENT_EXTRA_USER;
import static com.example.archivoappuser.Common.Common.KEY_QUERY_FILTER_USERS_ID;
import static com.example.archivoappuser.Common.Common.KEY_REGEX_1;
import static com.example.archivoappuser.Common.Common.KEY_REGEX_2;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_TOKENS;
import static com.example.archivoappuser.Common.Common.KEY_TABLE_USERS;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";
    private EditText edt_id;
    private Button btn_submit;
    public FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;

    private Query check_query;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private ValueEventListener check_Exist_Id_ValueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO Add mAuth Listener to check if user is loged in


        //TODO if not logged in

        //TODO Create method to check ID if exists .

        //TODO Hold the information in User  variable;


        edt_id = findViewById(R.id.edt_id_idactivity);
        btn_submit = findViewById(R.id.btn_submit_idactivity);

        //init firebase
        mAuth = FirebaseAuth.getInstance();

        mFirebaseDatabase = FirebaseDatabase.getInstance();

        mDatabaseReference = mFirebaseDatabase.getReference(KEY_TABLE_USERS);


        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait ..");
        mDialog.show();

        if (Common.IsConnectedToInternet(this)) {
            mDialog.show();
            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                    firebaseUser = firebaseAuth.getCurrentUser();
                    if (firebaseUser != null) {
                        final DatabaseReference ref = mDatabaseReference.child(firebaseUser.getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Log.d(TAG, "onDataChange: " + dataSnapshot.toString());
                                Common.CurrentUser = dataSnapshot.getValue(User.class);
                                FirebaseMessaging.getInstance().subscribeToTopic(Common.CurrentUser.getGroupId());
                                ref.child("online").setValue(true);
                                UpdateToken(FirebaseInstanceId.getInstance().getToken());
                                startApp();
                                mDialog.dismiss();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                mDialog.dismiss();
                            }
                        });
                    } else {
                        Log.d(TAG, "onAuthStateChanged: signed out");
                        mDialog.dismiss();
                    }

                }
            };
        }


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.show();
                checkValidation(edt_id);

            }
        });

    }

    private void startApp() {
        Intent i = new Intent(MainActivity.this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }


    private void UpdateToken(String token) {
        DatabaseReference tokens = mFirebaseDatabase.getReference(KEY_TABLE_TOKENS);
        Token tokenData = new Token(token, "false");
        tokens.child(Common.CurrentUser.getUserUid()).setValue(tokenData);
    }


    private boolean checkIdFormat(String str) {
        boolean check = true;
        if (str.matches(KEY_REGEX_1)) {
            check = false;
        } else if (str.matches(KEY_REGEX_2)) {
            check = false;
        }

        return check;
    }

    public void checkValidation(EditText editTextID) {


        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Please Wait ..");
        mDialog.show();


        String id;
        id = editTextID.getText().toString();
        if (id.equals("")) {
            editTextID.setError(getString(R.string.input_error_ID));
            editTextID.requestFocus();
            mDialog.dismiss();
            return;
        } else if (checkIdFormat(id)) {
            editTextID.setError(getString(R.string.input_error_Format_ID));
            editTextID.requestFocus();
            mDialog.dismiss();
            Log.d(TAG, "checkValidation: 224");
            return;
        }

        if (checkExistID(editTextID)) {
            mDialog.dismiss();
            editTextID.setError(getString(R.string.input_error_Not_Exist_ID));
            editTextID.requestFocus();

            Log.d(TAG, "checkValidation: 224");
        }

    }


    private void startApp(User user, String key) {
        Common.CurrentUser = user;
        Intent i;
        if (user.getUserUid().equals("")) {
            //Registration
            i = new Intent(MainActivity.this, RegisterActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("KEY", key);
            startActivity(i);
            finish();
        } else {
            //Login
            i = new Intent(MainActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

    }

    private Boolean checkExistID(final EditText edt_personid) {

        final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
        mDialog.setMessage("Checking ID ,Please Wait ..");
        final String ID = edt_personid.getText().toString();
        final boolean[] check = {false};
        DatabaseReference ref = mFirebaseDatabase.getReference(KEY_TABLE_USERS);
        check_query = ref.orderByChild(KEY_QUERY_FILTER_USERS_ID).equalTo(ID);
        check_Exist_Id_ValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: 63 " + ds.getValue(User.class).toString());
                    User user = ds.getValue(User.class);
                    if (user.getUserId().equals(edt_personid.getText().toString())) {
                        startApp(user, ds.getKey());
                        mDialog.dismiss();
                        finish();
                    } else {
                        edt_personid.setError(getString(R.string.input_error_Not_Exist_ID));
                        edt_personid.requestFocus();
                        mDialog.dismiss();
                    }

                }

                mDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mDialog.dismiss();
            }
        };
        mDialog.dismiss();

        check_query.addValueEventListener(check_Exist_Id_ValueEventListener);


        return check[0];

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (check_query != null) {
            check_query.removeEventListener(check_Exist_Id_ValueEventListener);
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
        if (check_query != null)
            check_query.removeEventListener(check_Exist_Id_ValueEventListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth.addAuthStateListener(mAuthListener);
    }
}
