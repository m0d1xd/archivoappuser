package com.example.archivoappuser.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private String userName, userId, userUid, email, password, groupId, imageUrl;
    private Boolean online;

    public User() {
    }

    protected User(Parcel in) {
        userName = in.readString();
        userId = in.readString();
        userUid = in.readString();
        email = in.readString();
        password = in.readString();
        groupId = in.readString();
        byte tmpOnline = in.readByte();
        online = tmpOnline == 0 ? null : tmpOnline == 1;
    }


    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(userId);
        dest.writeString(userUid);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(groupId);
        dest.writeByte((byte) (online == null ? 0 : online ? 1 : 2));
    }


    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", userId='" + userId + '\'' +
                ", userUid='" + userUid + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", groupId='" + groupId + '\'' +
                ", online=" + online +
                '}';
    }
}
