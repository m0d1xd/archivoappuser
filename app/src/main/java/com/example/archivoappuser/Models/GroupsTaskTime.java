package com.example.archivoappuser.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class GroupsTaskTime implements Parcelable {

    private long startTime, finishTime;
    private String status;

    public GroupsTaskTime() {
        this.startTime = 0;
        this.finishTime = 0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GroupsTaskTime(long startTime, long finishTime, String uid) {
        this.startTime = startTime;
        this.finishTime = finishTime;

    }

    protected GroupsTaskTime(Parcel in) {
        startTime = in.readLong();
        finishTime = in.readLong();
    }

    public static final Creator<GroupsTaskTime> CREATOR = new Creator<GroupsTaskTime>() {
        @Override
        public GroupsTaskTime createFromParcel(Parcel in) {
            return new GroupsTaskTime(in);
        }

        @Override
        public GroupsTaskTime[] newArray(int size) {
            return new GroupsTaskTime[size];
        }
    };

    public long getStartTime() {

        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(startTime);
        dest.writeLong(finishTime);
    }
}
