package com.example.archivoappuser.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class GroupsTasks implements Parcelable {

    private String task, groupId, status;
    private long deadline;
    private HashMap<String, GroupsTaskTime> timeTable;

    public GroupsTasks() {
    }

    protected GroupsTasks(Parcel in) {
        task = in.readString();
        groupId = in.readString();
        status = in.readString();
        deadline = in.readLong();
    }

    public static final Creator<GroupsTasks> CREATOR = new Creator<GroupsTasks>() {
        @Override
        public GroupsTasks createFromParcel(Parcel in) {
            return new GroupsTasks(in);
        }

        @Override
        public GroupsTasks[] newArray(int size) {
            return new GroupsTasks[size];
        }
    };

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public HashMap<String, GroupsTaskTime> getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(HashMap<String, GroupsTaskTime> timeTable) {
        this.timeTable = timeTable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(task);
        dest.writeString(groupId);
        dest.writeString(status);
        dest.writeLong(deadline);
    }
}


