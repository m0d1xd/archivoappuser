package com.example.archivoappuser.Models.NotificationModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class FCMResponce {

    @SerializedName("multicast_id")
    public long multicast_id;

    @SerializedName("success")
    public int success;

    @SerializedName("failure")
    public int failure;


    @SerializedName("canonical_ids")
    public int canonical_ids;

    public List<Result> results;

    @Override
    public String toString() {
        return "FCMResponce{" +
                "multicast_id=" + multicast_id +
                ", success=" + success +
                ", failure=" + failure +
                ", canonical_ids=" + canonical_ids +
                ", results=" + printResults() +
                '}';
    }

    public String printResults() {

        String t = "";
        for (Result result : this.results) {
            t += result.message_id + "\n";
        }
        return t;
    }
}
